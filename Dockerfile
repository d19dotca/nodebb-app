FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ARG VERSION=1.15.2

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# crashes with node 12
RUN mkdir -p /usr/local/node-10.19.0 && \
    curl -L https://nodejs.org/dist/v10.19.0/node-v10.19.0-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-10.19.0

ENV PATH /usr/local/node-10.19.0/bin:$PATH

RUN curl -L https://github.com/NodeBB/NodeBB/archive/v${VERSION}.tar.gz | tar -xz --strip-components 1 -f -
RUN cp /app/code/install/package.json /app/code/package.json
RUN npm install --production

# ~/.config is for npm update to work
# only package.json is preserved because nodebb doesn't seem to use lock files
RUN mv /app/code/node_modules /app/code/node_modules_copy && ln -s /run/nodebb/node_modules /app/code/node_modules && \
    mv /app/code/package-lock.json /app/code/package-lock.json.copy && ln -s /run/nodebb/package-lock.json /app/code/package-lock.json && \
    mv /app/code/package.json /app/code/package.json.copy && ln -s /app/data/package.json /app/code/package.json && \
	mv /app/code/public /app/code/public_template && ln -s /run/nodebb/public /app/code/public && \
	rm -rf /app/code/logs && ln -sf /run/nodebb/logs /app/code/logs && \
	ln -s /run/nodebb/config.json /app/code/config.json && \
	rm -rf /app/code/build && ln -s /run/nodebb/build /app/code/build && \
	rm -rf /home/cloudron/.npm && ln -s /run/nodebb/npm /home/cloudron/.npm && \
	rm -rf /home/cloudron/.node-gyp && ln -s /run/nodebb/node-gyp /home/cloudron/.node-gyp && \
	rm -rf /home/cloudron/.config && ln -s /run/nodebb/config /home/cloudron/.config && \
    rm -rf /home/cloudron/.cache && ln -s /run/nodebb/cache /home/cloudron/.cache

COPY config.json.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

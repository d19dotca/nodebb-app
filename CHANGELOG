[0.1.0]
* Initial version

[0.1.1]
* Update screenshots

[0.1.2]
* NodeBB 1.1.2

[0.1.3]
* Use screenshots from s3

[0.1.4]
* Update description

[0.1.5]
* Add customAuth and post install message

[0.2.0]
* Update to NodeBB 1.2.0

[0.2.1]
* Update to NodeBB 1.2.1

[0.2.2]
* Remove redundant CustomAuth flag

[0.3.0]
* Update to NodeBB 1.4.0

[0.4.0]
* Update to NodeBB 1.4.3
* Update base image to 0.10.0

[0.4.1]
* Update to NodeBB 1.4.6

[0.4.2]
* Re-update to NodeBB 1.4.6

[0.5.0]
* Update to NodeBB 1.5.0

[0.5.1]
* Update to NodeBB 1.5.1

[0.5.2]
* Update to NodeBB 1.5.1
* Fix random build issues

[1.0.0]
* Update to NodeBB 1.5.3

[1.1.0]
* Update to NodeBB 1.6.1

[1.2.0]
* Update NodeBB to 1.7.0 (100th release!)
* https://blog.nodebb.org/nodebb-1-7-0/
* https://community.nodebb.org/topic/11465/1-7-0-breaking-changes

[1.3.0]
* Update NodeBB to 1.7.2

[1.4.0]
* Update NodeBB to 1.7.3

[1.5.0]
* Update NodeBB to 1.7.4

[1.5.1]
* Update NodeBB to 1.7.5
* Fixes an [issue](https://github.com/NodeBB/NodeBB/issues/6306) where some bootswatch skins are broken

[1.5.2]
* Use NodeBB's built-in mailer for sending emails

[1.5.3]
* Use NodeBB's built-in mailer for sending emails

[1.6.0]
* Update NodeBB to 1.8.1
* [Release blog](https://community.nodebb.org/topic/12073/nodebb-v1-8-1-released)
* [Breaking changes](https://community.nodebb.org/topic/11864/1-8-0-breaking-changes/2)

[1.6.1]
* Update NodeBB to 1.8.2
* [Release announcement](https://community.nodebb.org/topic/12159/nodebb-v1-8-2-released)

[1.7.0]
* Update NodeBB to 1.9.0
* [Release announcements](https://community.nodebb.org/topic/12294/nodebb-v1-9-0-has-been-released)

[1.7.1]
* Update NodeBB to 1.9.1

[1.7.2]
* Update NodeBB to 1.9.2

[1.7.3]
* Update NodeBB to 1.9.3

[1.8.0]
* Update NodeBB to 1.10.0
* [Release announcement](https://blog.nodebb.org/nodebb-1-10-0/)
* Adding profile/cover photos can now be limited to users with a minimum reputation level
* IP address of registered users are now recorded in chat messages, and can be seen by admins and moderators
* An additional action hook has been added to the analytics increment event, making it easier to connect your forum to an external monitoring system
* Additional data is now sent to filter:search.query hook for better search engine implementation downstream)

[1.8.1]
* Update NodeBB to 1.10.1

[1.9.0]
* Use latest base image

[1.9.1]
* Update NodeBB to 1.10.2

[1.10.0]
* Update NodeBB to 1.11.0
* [Release announcement](https://blog.nodebb.org/nodebb-1-11-0-some-stuff-and-things/)
* search via the category dropdown in the unread, recent and composer views
* forum traffic filtering
* quickly move to any message in a topic
* category list optimizations
* ability to control widget visibility by group
* ability to sort search results by # of votes
* a page in the admin control panel to see all active hooks

[1.10.1]
* Update NodeBB to 1.11.1
* remove uid::ignored:cids (#7099) (263c918)
* cache category tag whitelist (78fa734)
* make user cards look less derpy (31bb2ae)
* added new middleware authenticateOrGuest (4fba149)
* Fix issue where plugins where not correctly re-installed

[1.10.2]
* Update NodeBB to 1.11.2

[1.11.0]
* Update NodeBB to 1.12.0
* admin/groups style change (2b6f1a0)
* add process cpu usage to admin (db47753)
* revamp email templates to be more style agnostic (#7375) (f32a992)
* lower search timeout (fc830c0)
* quick search (8a0e128)
* logging password resets and errors into event log (0c09b74)

[1.11.1]
* Update NodeBB to 1.12.1
* update unban logic/invocation and refactor User.bans module (3fbb6fa)
* add original sessionID to static:user.loggedOut (abe4abb)
* don't crash if templateData is undefined (eb2c3e5)

[1.11.2]
* Update NodeBB to 1.12.2

[1.11.3]
* Fix installation of plugins with native node modules

[1.12.0]
* Update NodeBB to 1.13.0
* [Full Changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.13.0)
* displaying one-click unsubscribe link in email footer (#8024) (df13992)
* #7467, pass query params when redirecting to posts (480a64a)
* use helpers.setupAdminPageRoute (b5a3000)
* wip, better digest handling (+ eventual digest resend logic) (#7995) (645d647)
* add action:messaging.save (ac5c560)
* #7957, allow post queue based on group (1cedc4a)
* add filter:topics.unreadCutoff (e020b85)
* Add filter:topic.delete and filter:topic.restore (#7946) (#7989) (989107d)
* no more session cookie for guests (#7982) (cf7e0cf)
* Implement WICG change-password-url (#7072) (#7990) (df1efe5)
* log errors from mubsub (b01a47c)
* upgrade to sitemap5 (#7980) (d679218)
* #7964, change all categories at once (485fbd2)
* closes #7952, translate widget-settings (990508a)
* remove ability to delete events from acp (554e671)
* resetting theme will reset skin (03827fa)

[1.12.1]
* Update NodeBB to 1.13.1

[1.13.0]
* Persist package.json to preserve plugin versions across updates

[1.13.1]
* Update NodeBB to 1.13.2

[1.13.2]
* Use `admin@server.local` as admin email for new installations

[1.13.3]
* Use manifest v2
* Update node to 10.19.0

[1.14.0]
* Use base image 2.0.0

[1.14.1]
* Update NodeBB to 1.13.3
* [Full changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.13.3)

[1.15.0]
* Update NodeBB to 1.14.2
* [Full changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.14.0)
* #8450, next/prev link tags on /unread /recent (eb9704f)
* allow flagging of user acounts from post tools menu (6931f29)
* #3783, min/max tags per category (c718b72)
* account content deletion, closes #8381 (67aca82)
* privileges for Admin Control Panel (#8355) (a82e9bd)

[1.16.0]
* Update NodeBB to 1.15.0
* [Full changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.15.0)

[1.16.1]
* Update NodeBB to 1.15.1
* [Full changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.15.1)
* #8864, add action:events.log (9c5c32d)
* show db info side by side (62c0454)
* add language keys for admin-settings-api (d32e4e0)
* #8824, cache refactor (#8851) (f1f9b22)
* move mkdirp to beforeBuild so it doesnt get called twice (6255874)
* group exists API call in write api (d263192)
* user exist route in write api (1446cec)
* new shorthand route /api/v3/users/bySlug/:userslug (60e1e99)
* allow passwords with length > 73 characters (#8818) (512f6de)
* #8821, allow guest topic views (9e3eb5d)

[1.16.2]
* Update NodeBB to 1.15.2
* [Full changelog](https://github.com/NodeBB/NodeBB/releases/tag/v1.15.2)
* #8475, allow flagging self posts (a6afcfd)
* #7550, show message if post is queued when js is disabled (120999b)
* #8171, add oldCategory if topic is moved (35f932c)
* #8204, separate notification type for group chats (b44ddec)
* invites regardless of registration type, invite privilege, groups to join on acceptance (#8786) (3ccebf1)
* allow groups to specify which cids to show member posts from (#8875) (8518404)

#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var server, browser = new Builder().forBrowser('chrome').build();
    var uploadedImageUrl;
    var username = 'admin', password = 'changeme123';

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    var app;
    var email, token;

    function login(username, password, done) {
        // browser.manage().window().setSize(1280,768).then(function () {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/login');
        }).then(function () {
            return browser.sleep(10000); // takes some time for username to be visible
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('username')), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('username')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.id('login')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "Announcements")]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkMailPlugin(done) {
        browser.get('https://' + app.fqdn + '/admin/settings/email').then(function () {
            return browser.wait(until.elementLocated(by.id('email:smtpTransport:host')), TEST_TIMEOUT);
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.findElement(by.id('email:smtpTransport:host')).getAttribute('value');
        }).then(function (val) {
            if (val !== 'mail') return done(new Error('Incorrect mail server value: ' + val));
            done();
        });
    }

    function restartForum(done) {
        browser.get('https://' + app.fqdn + '/admin').then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Restart"]')).click();
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Confirm"]')).click();
        }).then(function () {
            console.log('Waiting for forum to restart');
            return browser.sleep(50000); // wait for reload
        }).then(function () {
            return browser.get('https://' + app.fqdn + '/admin');
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//h1[text()="Dashboard"]')), TEST_TIMEOUT);
        }).then(function () { done(); });
    }

    function installCustomPlugin(done) {
        execSync('cloudron exec --app ' + app.id + ' -- /usr/local/bin/gosu cloudron:cloudron npm install nodebb-plugin-beep', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        done();
    }

    function activateCustomPlugin(done) {
        browser.get('https://' + app.fqdn + '/admin/extend/plugins#installed').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//ul[contains(@class, "installed")]//strong[text()="nodebb-plugin-beep"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            var button = browser.findElement(by.xpath('//li[@id="nodebb-plugin-beep"]//button[@data-action="toggleActive"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(by.xpath('//li[@id="nodebb-plugin-beep"]//button[@data-action="toggleActive"]')).click(); // activate the plugin
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            var button = browser.findElement(by.xpath('//button[text()="Confirm"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Confirm"]')).click();
        }).then(function () {
            return browser.sleep(20000); // wait for the action to succeed
        }).then(function() {
            done();
        });
    }

    function listCustomPlugin(done) {
        browser.get('https://' + app.fqdn + '/admin/extend/plugins#installed').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//strong[text()="nodebb-plugin-beep"]')), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function uploadImage(done) {
        browser.get('https://' + app.fqdn + '/user/admin/edit').then(function () {
            return browser.sleep(4000);
        }).then(function () {
            var button = browser.findElement(by.xpath('//a[text()="Change Picture"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(by.id('changePictureBtn')).click();
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            var button = browser.findElement(by.xpath('//button[@data-action="upload"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', button);
        }).then(function () {
            return browser.findElement(by.xpath('//button[@data-action="upload"]')).click();
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@type="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        }).then(function () {
            browser.sleep(10000);
        }).then(function () { // upload it
            return browser.findElement(by.id('fileUploadSubmitBtn')).click();
        }).then(function () {
            browser.sleep(10000);
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Crop and upload"]')).click();
        }).then(function () {
           return browser.sleep(10000);
        }).then(function () {
            done();
        });
    }

    function checkImage(done) {
        browser.get('https://' + app.fqdn + '/user/admin').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//img[@src="/assets/uploads/profile/1-profileavatar.png"]')), TEST_TIMEOUT);
        }).then(function () {
            var img = browser.findElement(by.xpath('//img[@src="/assets/uploads/profile/1-profileavatar.png"]'));
            return browser.executeScript('return arguments[0].complete && arguments[0].naturalWidth', img);
        }).then(function (imageWidth) {
            done(imageWidth === 200 ? null : new Error('failed to load image'));
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, username, password));
    it('check mail plugin', checkMailPlugin);
    it('can install custom plugin', installCustomPlugin);
    it('can restart forum', restartForum); // required before activate!
    it('can activate custom plugin', activateCustomPlugin);
    it('can list custom plugin', listCustomPlugin);
    it('can upload image', uploadImage);
    it('can check image', checkImage);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });


    it('can login', login.bind(null, username, password));
    it('can list custom plugin', listCustomPlugin);
    it('can check image', checkImage);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can login', login.bind(null, username, password));
    it('can list custom plugin', listCustomPlugin);
    it('can check image', checkImage);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, username, password));
    it('can list custom plugin', listCustomPlugin);
    it('can check image', checkImage);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.nodebb.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', login.bind(null, username, password));
    it('check mail plugin', checkMailPlugin);
    it('can install custom plugin', installCustomPlugin);
    it('can activate custom plugin', activateCustomPlugin);
    it('can upload image', uploadImage);
 
    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('check mail plugin', checkMailPlugin);
    it('can list custom plugin', listCustomPlugin);
    it('can check image', checkImage);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});

#!/bin/bash

set -eu

mongo_cli="mongo ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE} -u ${CLOUDRON_MONGODB_USERNAME} -p ${CLOUDRON_MONGODB_PASSWORD}"

echo "=> Creating directories"
mkdir -p /app/data/public/uploads /run/nodebb/logs /run/nodebb/public /run/nodebb/node_modules /run/nodebb/npm /run/nodebb/build /run/nodebb/config /run/nodebb/node-gyp /run/nodebb/cache

export NODE_ENV=production
export NODE_PATH=/app/code/node_modules

if [[ -z "$(ls -A /run/nodebb/public)" ]]; then
    echo "=> Copying public files"

    cp -rf /app/code/public_template/* /run/nodebb/public

    # symlink uploads for backup
    cp -rf /app/code/public_template/uploads/* /app/data/public/uploads
    rm -rf /run/nodebb/public/uploads
    ln -sf /app/data/public/uploads /run/nodebb/public/uploads

    # The public/ contains code that requires with relative links
    ln -s /app/code/src /run/nodebb/src
fi

# Some plugins write stuff into node_modules. Not nice :/
echo "=> Moving node_modules"
cp -rf /app/code/node_modules_copy/* /run/nodebb/node_modules
cp /app/code/package-lock.json.copy /run/nodebb/package-lock.json
# we don't care about preserving lockfile because nodebb doesn't use it
[[ ! -f /app/data/package.json ]] && cp /app/code/package.json.copy /app/data/package.json

chown -R cloudron:cloudron /app/data /run/nodebb

if [[ ! -f /app/data/.setup_done ]]; then
    echo "=> Running initial setup"
    setup="{
        \"url\": \"${CLOUDRON_APP_ORIGIN}\",
        \"admin:username\": \"admin\",
        \"admin:password\": \"changeme123\",
        \"admin:password:confirm\": \"changeme123\",
        \"admin:email\": \"admin@server.local\",
        \"database\": \"mongo\",
        \"mongo:host\": \"${CLOUDRON_MONGODB_HOST}\",
        \"mongo:port\": \"${CLOUDRON_MONGODB_PORT}\",
        \"mongo:username\": \"${CLOUDRON_MONGODB_USERNAME}\",
        \"mongo:password\": \"${CLOUDRON_MONGODB_PASSWORD}\",
        \"mongo:database\": \"${CLOUDRON_MONGODB_DATABASE}\"
    }"

    # this will create a config.json
    cd /app/code && /usr/local/bin/gosu cloudron:cloudron node /app/code/app --setup "${setup}"  --series
    touch /app/data/.setup_done
fi

# load session secret
if [[ -f /app/data/secret ]]; then
    secret=$(cat /app/data/secret)
else
    secret=$(uuid)
    echo "${secret}" > /app/data/secret
fi

# Re-create config.json
sed -e "s,##APP_ORIGIN,${CLOUDRON_APP_ORIGIN}," \
    -e "s/##MONGODB_HOST/${CLOUDRON_MONGODB_HOST}/" \
    -e "s/##MONGODB_PORT/${CLOUDRON_MONGODB_PORT}/" \
    -e "s/##MONGODB_USERNAME/${CLOUDRON_MONGODB_USERNAME}/" \
    -e "s/##MONGODB_PASSWORD/${CLOUDRON_MONGODB_PASSWORD}/" \
    -e "s/##MONGODB_DATABASE/${CLOUDRON_MONGODB_DATABASE}/" \
    -e "s/##SECRET/${secret}/" \
    /app/pkg/config.json.template > /run/nodebb/config.json

echo "Setting up email"
${mongo_cli} --eval "db.objects.update({ _key: 'config' }, { \$set: { 'email:smtpTransport:enabled': '1', 'email:smtpTransport:service': 'nodebb-custom-smtp', 'email:smtpTransport:host': '${CLOUDRON_MAIL_SMTP_SERVER}', 'email:smtpTransport:port': '${CLOUDRON_MAIL_SMTP_PORT}', 'email:smtpTransport:user': '${CLOUDRON_MAIL_SMTP_USERNAME}', 'email:smtpTransport:pass': '${CLOUDRON_MAIL_SMTP_PASSWORD}', 'email:smtpTransport:security': 'NONE', 'email:from': '${CLOUDRON_MAIL_FROM}' } }, { upsert: true })"

## --package will "merge" the /app/data/package.json with /app/code/install/package.json and preserve plugin modules
# note that the --plugin requires access to the database. we can use --install to install base modules but this
# is not needed because we copy modules from the docker image. only plugins needs to be re-installed
# for some reason, the merge does not remove the obsolete packages! for this reason we have to use dep-check=false below
echo "=> updating old package.json. this preserves plugin versions"
series=1 /usr/local/bin/gosu cloudron:cloudron /app/code/nodebb upgrade --package

# install plugins. one can always fixup package.json for "strict" plugin versions
# nodebb plugins command will list all types of plugins and inactive ones as well
echo "=> installing plugin (plugin|theme|widget|reward) modules"
for plugin in $(./nodebb plugins | grep 'nodebb-' | cut -f2 -d' '); do
    # this checks runtime node_modules, so that we don't do this on every restart
    if [[ ! -d "/run/nodebb/node_modules/${plugin}" ]]; then
        echo "=> Could not find plugin ${plugin}. Installing it."
        # note that without --save, npm install will remove the previous plugins!
        cd /app/code && /usr/local/bin/gosu cloudron:cloudron npm install --save "${plugin}"
    fi
done

# there are 3 binaries
# nodebb - this is the CLI used for controlling nodebb (install/build/active/start/stop/restart) etc
# loader - this is what "nodebb start" starts. loader spawns 'n' number of app(s). using the loader even with 1 app
#          is required for things like restart/reload to work from the NodeBB admin panel
# app    - this is the actual app.

echo "=> Updating nodebb database and building assets"
series=1 /usr/local/bin/gosu cloudron:cloudron /app/code/nodebb upgrade --schema --build

echo "=> Starting nodebb"
# export NODE_ENV=dev for verbose logging
# dep-check=false means nodebb won't scan package.json and look for all modules on startup. this is required because package.json can
# contain obsolete packages i.e not removed by upgrade --package. sadly, there is a bug that env vars are not sent from loader to app
# and secondly env vars in nconf are parsed as strings and not boolean. this means the dep-check compare in src/start.js fails
# for this reason, we patch the code for now (in Dockerfile)
series=1 exec /usr/local/bin/gosu cloudron:cloudron node /app/code/loader.js --no-daemon --no-silent
